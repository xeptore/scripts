# Scripts

Some scripts I usually use.

- Upstream server using Cloudflare Warp:

    ```sh
    curl -sSfLO https://gitlab.com/xeptore/scripts/-/raw/main/vpn/server-cf.sh
    # edit the script if you need to
    warp_license_key=key proxy_domain=domain bash ./server-cf.sh
    ```

- neko:

    ```sh
    curl -sSfL https://gitlab.com/xeptore/scripts/-/raw/main/neko.sh | bash -s
    ```
