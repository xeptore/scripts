#!/bin/bash

set -eo pipefail

bail() {
  echo 'Error executing command, exiting'
  exit 1
}

exec_cmd_nobail() {
  echo "+ $1"
  bash -c "$1"
}

exec_cmd() {
  exec_cmd_nobail "$1" || bail
}

export DEBIAN_FRONTEND=noninteractive

#
# Setup Docker (https://docs.docker.com/engine/install/ubuntu/)
#
exec_cmd 'apt-get -qq update'
exec_cmd 'apt-get -qqy install ca-certificates curl gnupg'
exec_cmd 'install -m 0755 -d /etc/apt/keyrings'
exec_cmd 'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg'
exec_cmd 'chmod a+r /etc/apt/keyrings/docker.gpg'
exec_cmd "echo deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(source /etc/os-release && echo "$VERSION_CODENAME") stable | tee /etc/apt/sources.list.d/docker.list >/dev/null"
exec_cmd 'apt-get -qq update'
exec_cmd 'apt-get -qqy install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin'
exec_cmd 'docker container run --rm hello-world'
#
# End Setup Docker
#

#
# Setup Unbound (https://martinkubecka.github.io/posts/general/recursive_dns/)
#
exec_cmd 'apt -qqy install unbound net-tools'
cat >/etc/unbound/unbound.conf.d/pi-hole.conf <<EOF
server:
    # If no logfile is specified, syslog is used
    #logfile: "/var/log/unbound/unbound.log"
    verbosity: 0

    interface: 127.0.0.1
    port: 5335
    do-ip4: yes
    do-udp: yes
    do-tcp: yes

    # May be set to yes if you have IPv6 connectivity
    do-ip6: no

    # You want to leave this to no unless you have *native* IPv6. With 6to4 and
    # Terredo tunnels your web browser should favor IPv4 for the same reasons
    prefer-ip6: no

    # Use this only when you downloaded the list of primary root servers!
    # If you use the default dns-root-data package, unbound will find it automatically
    #root-hints: "/var/lib/unbound/root.hints"

    # Trust glue only if it is within the server's authority
    harden-glue: yes

    # Require DNSSEC data for trust-anchored zones, if such data is absent, the zone becomes BOGUS
    harden-dnssec-stripped: yes

    # Don't use Capitalization randomization as it known to cause DNSSEC issues sometimes
    # see https://discourse.pi-hole.net/t/unbound-stubby-or-dnscrypt-proxy/9378 for further details
    use-caps-for-id: no

    # Reduce EDNS reassembly buffer size.
    # IP fragmentation is unreliable on the Internet today, and can cause
    # transmission failures when large DNS messages are sent via UDP. Even
    # when fragmentation does work, it may not be secure; it is theoretically
    # possible to spoof parts of a fragmented DNS message, without easy
    # detection at the receiving end. Recently, there was an excellent study
    # >>> Defragmenting DNS - Determining the optimal maximum UDP response size for DNS <<<
    # by Axel Koolhaas, and Tjeerd Slokker (https://indico.dns-oarc.net/event/36/contributions/776/)
    # in collaboration with NLnet Labs explored DNS using real world data from the
    # the RIPE Atlas probes and the researchers suggested different values for
    # IPv4 and IPv6 and in different scenarios. They advise that servers should
    # be configured to limit DNS messages sent over UDP to a size that will not
    # trigger fragmentation on typical network links. DNS servers can switch
    # from UDP to TCP when a DNS response is too big to fit in this limited
    # buffer size. This value has also been suggested in DNS Flag Day 2020.
    edns-buffer-size: 1232

    # Perform prefetching of close to expired message cache entries
    # This only applies to domains that have been frequently queried
    prefetch: yes

    # One thread should be sufficient, can be increased on beefy machines. In reality for most users running on small networks or on a single machine, it should be unnecessary to seek performance enhancement by increasing num-threads above 1.
    num-threads: 4

    # Ensure kernel buffer is large enough to not lose messages in traffic spikes
    so-rcvbuf: 10m

    # Ensure privacy of local IP ranges
    private-address: 192.168.0.0/16
    private-address: 169.254.0.0/16
    private-address: 172.16.0.0/12
    private-address: 10.0.0.0/8
    private-address: fd00::/8
    private-address: fe80::/10
EOF

cat >/etc/sysctl.d/99-proxy-tune.conf <<EOF
net.ipv4.ip_forward = 1
net.ipv4.ip_no_pmtu_disc = 1
fs.file-max = 51200
net.core.rmem_max = 67108864
net.core.wmem_max = 67108864
net.core.netdev_max_backlog = 250000
net.core.somaxconn = 4096
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_tw_recycle = 0
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_time = 1200
net.ipv4.tcp_max_syn_backlog = 8192
net.ipv4.tcp_max_tw_buckets = 5000
net.ipv4.tcp_fastopen = 3
net.ipv4.tcp_mem = 25600 51200 102400
net.ipv4.tcp_rmem = 4096 87380 67108864
net.ipv4.tcp_wmem = 4096 65536 67108864
net.ipv4.tcp_mtu_probing = 1
net.core.default_qdisc = fq
net.ipv4.tcp_congestion_control=bbr
net.ipv4.tcp_fastopen = 3
EOF

exec_cmd 'sysctl -p'

exec_cmd 'systemctl enable unbound.service'
exec_cmd 'systemctl restart unbound.service'
#
# End Setup Unbound
#

#
# Setup Pi-hole (https://github.com/pi-hole/docker-pi-hole#readme)
#
exec_cmd 'apt-get -qqy install diceware'

web_password=$(diceware --no-caps -d '-' --dice-sides 8192 -n 6)

exec_cmd 'mkdir -vp pihole/etc/{dnsmasq.d,pihole}/'

pihole_listen_iface=$(route -n | awk '$1 == "0.0.0.0" {print $8}')

cat >./pihole/etc/dnsmasq.d/listen.conf <<EOF
except-interface=$pihole_listen_iface
listen-address=127.0.0.1
bind-interfaces
EOF

cat >./pihole/compose.yml <<EOF
name: pihole
services:
  pihole:
    image: pihole/pihole:latest
    network_mode: host
    environment:
      TZ: Etc/Utc
      WEBPASSWORD: '${web_password}'
      WEBTHEME: default-darker
      PIHOLE_DNS_: '127.0.0.1#5335'
      DNSMASQ_LISTENING: local
      WEB_PORT: 8090
      WEB_BIND_ADDR: 127.0.0.1
    volumes:
      - ./etc/pihole:/etc/pihole
      - ./etc/dnsmasq.d:/etc/dnsmasq.d
    restart: unless-stopped
EOF

exec_cmd 'cd ./pihole'
exec_cmd 'docker compose -f ./compose.yml up -d'
exec_cmd 'cd -'
#
# End Setup Pi-hole
#

#
# Setup Sing-box
#
exec_cmd 'curl -SfLO https://github.com/z4x7k/sing-box-all/releases/latest/download/sing-box && chmod +x ./sing-box'

read -rp 'Warp License Key: ' warp_license_key

cat >./warp.sh <<EOF
#!/bin/bash

set -e

apt update && apt upgrade -y && apt install curl -y

curl -sSfL https://github.com/ViRb3/wgcf/releases/download/v2.2.17/wgcf_2.2.17_linux_amd64 -o ./wgcf && chmod +x ./wgcf

./wgcf register --accept-tos

WGCF_LICENSE_KEY="$warp_license_key" ./wgcf update

./wgcf generate
EOF

exec_cmd 'touch warp.conf'

exec_cmd "docker container run --rm -it --mount=type=bind,source=$(pwd)/warp.sh,target=/warp.sh,readonly --mount=type=bind,source=$(pwd)/warp.conf,target=/wgcf-profile.conf ubuntu bash /warp.sh"

private_key=$(grep 'PrivateKey = ' ./warp.conf | cut -d ' ' -f 3)
public_key=$(grep 'PublicKey = ' ./warp.conf | cut -d ' ' -f 3)
addresses=$(grep 'Address = ' ./warp.conf | cut -d ' ' -f 3 | sed s/^/'"'/ | sed s/'$'/'"'/ | paste -sd ',' -)

ws_path=$(diceware --no-caps -d '-' --dice-sides 8192 -n 8)
wg_user_name=$(diceware --no-caps --dice-sides 8192 -n 1)
wg_user_uuid=$(uuidgen)
direct_user_name=$(diceware --no-caps --dice-sides 8192 -n 1)
direct_user_uuid=$(uuidgen)

cat >./config.json <<EOF
{
  "dns": {
    "servers": [
      {
        "tag": "local",
        "address": "udp://127.0.0.1",
        "strategy": "ipv4_only"
      },
      {
        "tag": "cf",
        "address": "udp://1.1.1.1",
        "strategy": "ipv4_only"
      }
    ],
    "rules": [
      {
        "auth_user": [
          "${wg_user_name}"
        ],
        "server": "cf"
      },
      {
        "auth_user": [
          "${direct_user_name}"
        ],
        "server": "local"
      }
    ]
  },
  "log": {
    "level": "warn",
    "timestamp": true
  },
  "inbounds": [
    {
      "type": "mixed",
      "tag": "mixed-in",
      "listen": "127.0.0.1",
      "listen_port": 10801
    },
    {
      "type": "vmess",
      "tag": "vmess-in",
      "listen": "127.0.0.1",
      "listen_port": 2001,
      "tcp_fast_open": true,
      "udp_fragment": false,
      "users": [
        {
          "name": "${wg_user_name}",
          "uuid": "${wg_user_uuid}",
          "alterId": 0
        },
        {
          "name": "${direct_user_name}",
          "uuid": "${direct_user_uuid}",
          "alterId": 0
        }
      ],
      "sniff": false,
      "sniff_override_destination": false,
      "transport": {
        "type": "ws",
        "path": "/${ws_path}",
        "max_early_data": 2048
      }
    }
  ],
  "outbounds": [
    {
      "type": "wireguard",
      "tag": "wg",
      "interface_name": "wg0",
      "system_interface": false,
      "local_address": [
        $addresses
      ],
      "private_key": "$private_key",
      "server": "162.159.193.10",
      "server_port": 2408,
      "peer_public_key": "$public_key",
      "reserved": "AAAA"
    },
    {
      "type": "direct",
      "tag": "direct"
    },
    {
      "type": "dns",
      "tag": "dns-out"
    },
    {
      "type": "block",
      "tag": "block"
    }
  ],
  "route": {
    "rules": [
      {
        "protocol": "dns",
        "outbound": "dns-out"
      },
      {
        "auth_user": [
          "${wg_user_name}"
        ],
        "outbound": "wg"
      },
      {
        "auth_user": [
          "${direct_user_name}"
        ],
        "outbound": "direct"
      }
    ]
  }
}
EOF

exec_cmd './sing-box check --config ./config.json'
exec_cmd './sing-box format --write --config ./config.json'

cat >/etc/systemd/system/sing-box.service <<EOF
[Unit]
Description=Sing-box Service
Documentation=https://github.com/SagerNet/sing-box
After=network.target nss-lookup.target

[Service]
ExecStart=$(pwd)/sing-box run --config $(pwd)/config.json
Restart=on-failure
RestartSec=10
RestartPreventExitStatus=23
LimitNPROC=infinity
LimitNOFILE=infinity

[Install]
WantedBy=multi-user.target
EOF

exec_cmd 'systemctl daemon-reload'
exec_cmd 'systemctl enable sing-box.service'
exec_cmd 'systemctl restart sing-box.service'
#
# End Setup Sing-box
#

#
# Setup Caddy (https://caddyserver.com/docs/install#debian-ubuntu-raspbian)
#
exec_cmd 'apt-get -qqy install debian-keyring debian-archive-keyring apt-transport-https'
exec_cmd 'curl -1sLf https://dl.cloudsmith.io/public/caddy/stable/gpg.key | gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg'
exec_cmd 'curl -1sLf https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt | tee /etc/apt/sources.list.d/caddy-stable.list'
exec_cmd 'apt-get -qq update'
exec_cmd 'apt-get -qqy install caddy'

read -rp 'Proxy Domain: ' proxy_domain
read -rp 'Pi-hole Domain: ' pihole_domain

cat >/etc/caddy/Caddyfile <<EOF
${proxy_domain} {
	encode gzip
	reverse_proxy /${ws_path}* http://127.0.0.1:2001 {
		header_up Host {upstream_hostport}
		header_up -Origin
	}
}

${pihole_domain} {
	redir / /admin/ permanent
	encode gzip
	reverse_proxy /admin/* http://127.0.0.1:8090
}
EOF

exec_cmd 'caddy fmt --overwrite /etc/caddy/Caddyfile'
exec_cmd 'systemctl enable caddy.service'
exec_cmd 'systemctl restart caddy.service'
#
# End Setup Caddy
#

cat <<EOC
Proxy:
  Address: ${proxy_domain}:443
  WS PATH: /$ws_path
  WG User:
    Name: $wg_user_name
    UUID: $wg_user_uuid
  Direct User:
    Name: $direct_user_name
    UUID: $direct_user_uuid

Pi-hole:
  Address: https://${pihole_domain}
  Password: $web_password

Firewall:
  Block ICMP (ping) requests: https://bobcares.com/blog/ufw-block-ping/

Change SSH Port:
  mkdir -p /etc/systemd/system/ssh.socket.d
  cat> /etc/systemd/system/ssh.socket.d/listen.conf <<EOF
[Socket]
ListenStream=
ListenStream=35427
EOF
  systemctl restart ssh.socket
EOC
