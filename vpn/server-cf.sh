#!/bin/bash

for v in warp_license_key proxy_domain; do
  if [[ ! -v $v ]]; then
    echo "$v is required to be set."
    exit 2
  fi
done

set -ueo pipefail

bail() {
  echo 'Error executing command, exiting'
  exit 1
}

exec_cmd_nobail() {
  echo "+ $1"
  bash -c "$1"
}

exec_cmd() {
  exec_cmd_nobail "$1" || bail
}

export DEBIAN_FRONTEND=noninteractive

install_base() {
  exec_cmd 'apt-get -qq update'
  exec_cmd 'apt-get -qqy install ca-certificates curl gnupg net-tools unzip nethogs diceware jq'
}

install_docker() {
  exec_cmd 'apt-get -qq update'
  exec_cmd 'apt-get -qqy install ca-certificates curl gnupg'
  exec_cmd 'install -m 0755 -d /etc/apt/keyrings'
  exec_cmd 'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --batch --yes --dearmor --output /etc/apt/keyrings/docker.gpg'
  exec_cmd 'chmod a+r /etc/apt/keyrings/docker.gpg'
  exec_cmd "echo deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(source /etc/os-release && echo "$VERSION_CODENAME") stable | tee /etc/apt/sources.list.d/docker.list >/dev/null"
  exec_cmd 'apt-get -qq update'
  exec_cmd 'apt-get -qqy install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin'
  exec_cmd 'docker container run --rm hello-world'
  exec_cmd 'systemctl disable docker.service docker.socket'
}

install_singbox() {
  exec_cmd_nobail 'docker info' || install_docker
  exec_cmd_nobail './sing-box version' || exec_cmd 'curl -SfLO https://github.com/z4x7k/sing-box-all/releases/latest/download/sing-box && chmod +x ./sing-box'
  cat >./warp.sh <<EOF
#!/bin/bash
set -e
apt update && apt upgrade -y && apt install curl -y
curl -sSfL https://github.com/ViRb3/wgcf/releases/download/v2.2.19/wgcf_2.2.19_linux_amd64 -o ./wgcf && chmod +x ./wgcf
./wgcf register --accept-tos
WGCF_LICENSE_KEY="${warp_license_key:?}" ./wgcf update
./wgcf generate
EOF
  exec_cmd 'touch warp.conf'
  exec_cmd "docker container run --rm --mount=type=bind,source=$(pwd)/warp.sh,target=/warp.sh,readonly --mount=type=bind,source=$(pwd)/warp.conf,target=/wgcf-profile.conf docker.io/library/ubuntu:rolling bash /warp.sh"
  local warp_private_key
  warp_private_key=$(grep 'PrivateKey = ' ./warp.conf | cut -d ' ' -f 3)
  local warp_public_key
  warp_public_key=$(grep 'PublicKey = ' ./warp.conf | cut -d ' ' -f 3)
  local warp_addresses
  warp_addresses=$(grep 'Address = ' ./warp.conf | cut -d ' ' -f 3 | sed s/^/'"'/ | sed s/'$'/'"'/ | paste -sd ',' -)
  vmess_ws_path=$(diceware --no-caps -d '-' --dice-sides 8192 -n 8)
  vless_ws_path=$(diceware --no-caps -d '-' --dice-sides 8192 -n 8)
  local user_name
  user_name=$(diceware --no-caps --dice-sides 8192 -n 1)
  user_uuid=$(uuidgen)
  cat >./config.json <<EOF
{
  "log": {
    "level": "warn",
    "timestamp": true
  },
  "dns": {
    "servers": [
      {
        "tag": "wg",
        "address": "https://1.1.1.1/dns-query",
        "strategy": "prefer_ipv4",
        "detour": "wg"
      }
    ],
    "rules": [
      {
        "outbound": [
          "wg"
        ],
        "server": "wg"
      }
    ]
  },
  "inbounds": [
    {
      "type": "mixed",
      "tag": "mixed-in",
      "listen": "127.0.0.1",
      "listen_port": 10801
    },
    {
      "type": "vmess",
      "tag": "vmess-in",
      "listen": "127.0.0.1",
      "listen_port": 2001,
      "tcp_fast_open": true,
      "udp_fragment": false,
      "users": [
        {
          "name": "${user_name}",
          "uuid": "${user_uuid}",
          "alterId": 0
        }
      ],
      "sniff": true,
      "sniff_override_destination": true,
      "transport": {
        "type": "ws",
        "path": "/${vmess_ws_path}",
        "max_early_data": 2048
      }
    },
    {
      "type": "vless",
      "tag": "vless-in",
      "listen": "127.0.0.1",
      "listen_port": 2002,
      "tcp_fast_open": true,
      "udp_fragment": false,
      "users": [
        {
          "name": "${user_name}",
          "uuid": "${user_uuid}",
          "flow": ""
        }
      ],
      "sniff": true,
      "sniff_override_destination": true,
      "transport": {
        "type": "ws",
        "path": "/${vless_ws_path}",
        "max_early_data": 2048
      }
    }
  ],
  "outbounds": [
    {
      "type": "wireguard",
      "tag": "wg",
      "interface_name": "wg0",
      "system_interface": false,
      "local_address": [
        $warp_addresses
      ],
      "private_key": "$warp_private_key",
      "server": "162.159.193.10",
      "server_port": 2408,
      "peer_public_key": "$warp_public_key",
      "reserved": "AAAA"
    },
    {
      "type": "dns",
      "tag": "dns-out"
    },
    {
      "type": "block",
      "tag": "block"
    }
  ],
  "route": {
    "rules": [
      {
        "geosite": [
          "private"
        ],
        "outbound": "block"
      },
      {
        "protocol": "dns",
        "outbound": "dns-out"
      },
      {
        "inbound": [
          "vmess-in",
          "vless-in"
        ],
        "outbound": "wg"
      }
    ]
  }
}
EOF

  exec_cmd './sing-box check --config ./config.json'
  exec_cmd './sing-box format --write --config ./config.json'
  cat >/etc/systemd/system/sing-box.service <<EOF
[Unit]
Description=Sing-box Service
Documentation=https://github.com/SagerNet/sing-box
After=network.target nss-lookup.target

[Service]
ExecStart=$(pwd)/sing-box run --config $(pwd)/config.json
Restart=on-failure
RestartSec=10
RestartPreventExitStatus=23
LimitNPROC=infinity
LimitNOFILE=infinity

[Install]
WantedBy=multi-user.target
EOF

  exec_cmd 'systemctl daemon-reload'
  exec_cmd 'systemctl enable sing-box.service'
  exec_cmd 'systemctl restart sing-box.service'
}

install_caddy() {
  exec_cmd 'apt-get -qqy install debian-keyring debian-archive-keyring apt-transport-https'
  exec_cmd 'curl -1sLf https://dl.cloudsmith.io/public/caddy/stable/gpg.key | gpg --batch --yes --dearmor --output /usr/share/keyrings/caddy-stable-archive-keyring.gpg'
  exec_cmd 'curl -1sLf https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt | tee /etc/apt/sources.list.d/caddy-stable.list'
  exec_cmd 'apt-get -qq update && apt-get -qqy install caddy'

  cat >/etc/caddy/Caddyfile <<EOF
${proxy_domain:?} {
  encode gzip
  reverse_proxy /${vmess_ws_path}* http://127.0.0.1:2001 {
    header_up Host {upstream_hostport}
    header_up -Origin
  }
  reverse_proxy /${vless_ws_path}* http://127.0.0.1:2002 {
    header_up Host {upstream_hostport}
    header_up -Origin
  }
}
EOF

  exec_cmd 'caddy fmt --overwrite /etc/caddy/Caddyfile'
  exec_cmd 'systemctl enable caddy.service'
  exec_cmd 'systemctl restart caddy.service'
}

install_base
install_singbox
install_caddy

cat <<EOF
uui: $user_uuid
vmess path: $vmess_ws_path
vless path: $vless_ws_path
EOF
