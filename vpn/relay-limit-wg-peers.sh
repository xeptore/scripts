#!/bin/bash

set -eo pipefail

no_limits=('10.0.0.6/32' '10.0.0.12/32' '10.0.0.10/32' '10.0.0.18/32' '10.0.0.36/32' '10.0.0.28/32' '10.0.0.39/32')
max_bandwidth='3mbit'

bail() {
  echo 'Error executing command, exiting'
  exit 1
}

exec_cmd_nobail() {
  echo "+ $1"
  bash -c "$1"
}

exec_cmd() {
  exec_cmd_nobail "$1" || bail
}

is_limited() {
  local ip needle="$1"
  for ip in "${no_limits[@]}"; do [[ "$ip" == "$needle" ]] && return 1; done
  return 0
}

exec_cmd_nobail 'tc qdisc del dev wg0 parent root'
exec_cmd 'tc qdisc add dev wg0 parent root handle 1: hfsc default 1'
exec_cmd 'tc class add dev wg0 parent 1: classid 1:1 hfsc ls rate 1gbit'

classid=2
for ip in $(wg show wg0 allowed-ips | cut -f 2); do
  if (is_limited "$ip"); then
    clsid=$((classid++))
    exec_cmd "tc class add dev wg0 parent 1: classid 1:$clsid hfsc ls rate $max_bandwidth ul rate $max_bandwidth"
    exec_cmd "tc filter add dev wg0 parent 1: protocol ip prio 1 u32 match ip dst $ip classid 1:$clsid"
  fi
done
