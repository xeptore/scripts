#!/bin/bash

set -eo pipefail

exec_cmd() {
  echo "+ $1"
  bash -c "$1"
}

install_docker() {
  exec_cmd 'apt-get -qq update'
  exec_cmd 'apt-get -qqy upgrade'
  exec_cmd 'apt-get -qqy install ca-certificates curl gnupg'
  exec_cmd 'install -m 0755 -d /etc/apt/keyrings'
  exec_cmd 'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg'
  exec_cmd 'chmod a+r /etc/apt/keyrings/docker.gpg'
  exec_cmd "echo deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(source /etc/os-release && echo "$VERSION_CODENAME") stable | tee /etc/apt/sources.list.d/docker.list >/dev/null"
  exec_cmd 'apt-get -qq update'
  exec_cmd 'apt-get -qqy upgrade'
  exec_cmd 'apt-get -qqy install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin'
  exec_cmd 'docker container run --rm hello-world'
}

exec_cmd 'docker info' || install_docker

cat >./brave-policies.json <<EOF
{
  "AutofillAddressEnabled": false,
  "AutofillCreditCardEnabled": false,
  "BrowserSignin": 0,
  "BrowserGuestModeEnforced": false,
  "AllowDinosaurEasterEgg": false,
  "BraveWalletDisabled": true,
  "BraveRewardsDisabled": true,
  "BrowserGuestModeEnabled": true,
  "DefaultNotificationsSetting": 2,
  "DeveloperToolsAvailability": 3,
  "ShowFullUrlsInAddressBar": true,
  "EditBookmarksEnabled": true,
  "FullscreenAllowed": true,
  "IncognitoModeAvailability": 0,
  "SyncDisabled": false,
  "AutoplayAllowed": true,
  "BrowserAddPersonEnabled": true,
  "DefaultPopupsSetting": 2,
  "DownloadRestrictions": 0,
  "VideoCaptureAllowed": true,
  "AllowFileSelectionDialogs": true,
  "PromptForDownloadLocation": true,
  "BookmarkBarEnabled": true,
  "PasswordManagerEnabled": false,
  "BrowserLabsEnabled": false,
  "DefaultGeolocationSetting": 2,
  "URLAllowlist": [
    "file:///home/neko/Downloads"
  ],
  "URLBlocklist": [
    "file://*",
    "brave://policy"
  ],
  "DefaultCookiesSetting": 4,
  "ExtensionInstallForcelist": [],
  "ExtensionInstallAllowlist": [],
  "ExtensionInstallBlocklist": []
}
EOF

install_diceware() {
  exec_cmd 'apt-get -qqy install diceware'
}

exec_cmd 'diceware --version' || install_diceware

neko_pass=$(diceware --no-caps -d '-' --dice-sides 8192 -n 5)
admin_pass=$(diceware --no-caps -d '-' --dice-sides 8192 -n 5)

cat >./compose-neko.yml <<EOF
name: neko
services:
  brave:
    image: ghcr.io/m1k1o/neko/brave:latest
    restart: unless-stopped
    ports:
      - 127.0.0.1:6560:8080
      - 6561:6561/tcp
      - 6562:6562/udp
    cap_add:
      - SYS_ADMIN
    volumes:
      - type: bind
        source: ./brave-policies.json
        target: /etc/brave/policies/managed/policies.json
        read_only: true
    environment:
      NEKO_SCREEN: 1920x1080@60
      NEKO_PASSWORD: $neko_pass
      NEKO_PASSWORD_ADMIN: $admin_pass
      NEKO_TCPMUX: 6561
      NEKO_UDPMUX: 6562
      NEKO_ICELITE: true
EOF

exec_cmd 'docker compose -f ./compose-neko.yml up -d'

install_caddy() {
  #
  # Setup Caddy (https://caddyserver.com/docs/install#debian-ubuntu-raspbian)
  #
  exec_cmd 'apt-get -qqy install debian-keyring debian-archive-keyring apt-transport-https'
  exec_cmd 'curl -1sLf https://dl.cloudsmith.io/public/caddy/stable/gpg.key | gpg --dearmor -o /usr/share/keyrings/caddy-stable-archive-keyring.gpg'
  exec_cmd 'curl -1sLf https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt | tee /etc/apt/sources.list.d/caddy-stable.list'
  exec_cmd 'apt-get -qq update'
  exec_cmd 'apt-get -qqy upgrade'
  exec_cmd 'apt-get -qqy install caddy'
  exec_cmd 'systemctl enable caddy.service'
}

exec_cmd 'caddy version' || install_caddy

cat <<EOB



====================================================
cat >/etc/caddy/Caddyfile <<EOF
neko.com {
  reverse_proxy localhost:6560 {
    header_up Host {host}
    header_up X-Real-IP {remote_host}
    header_up X-Forwarded-For {remote_host}
    header_up X-Forwarded-Proto {scheme}
  }
}
EOF
caddy fmt --overwrite /etc/caddy/Caddyfile
systemctl restart caddy.service
====================================================



EOB
